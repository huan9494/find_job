module FindjobHelper
  DOMAIN = 'https://jp.indeed.com/'
  PRE_URL = 'https://jp.indeed.com/求人?q='
  PAGE = '&start='
  PRE_JOB_HREF = 'https://jp.indeed.com'

  require 'rubygems'
  require 'mechanize'
  require 'csv'

  def initialize_job
    @init = Mechanize.new
    @keyword = params[:query].downcase
    session[:keyword] = @keyword if @keyword
    @joblink = Joblink.where(keyword: @keyword).first
    
    @page_index = 0

    @job_title_arr = []
    @job_href_arr = []
    @company_name_arr = []
    @location_arr = []
    @job_description_arr = []

    @keyword = params[:query]
    @domain = DOMAIN
  end

  def update_job
    while @page_index < 1100
      url = PRE_URL + @keyword + PAGE + @page_index.to_s
      page = @init.get url
      results = page.search('div.row.result')
      href_arr = Joblink.all.collect(&:job_href)
      if results.present?

        results.each do |result|
        href = PRE_JOB_HREF + result.search('h2 a').attribute('href').text.strip
          unless href_arr.include?(href) ||
            @job_href_arr.include?(href)
            
            insert_data_to_array(result)
          end
        end

      else
        next
      end

      @page_index += 10

    end

    insert_data_to_database

  end

  def create_job
    while @page_index < 1100
      url = PRE_URL + @keyword + PAGE + @page_index.to_s
      page = @init.get url
      results = page.search('div.row.result')
      
      if results.present?

        results.each do |result|
        href = PRE_JOB_HREF + result.search('h2 a').attribute('href').text.strip
          unless @job_href_arr.include?(href)
            insert_data_to_array(result)
          end

        end
      else
        next
      end

      @page_index += 10

    end

    insert_data_to_database

  end

  def insert_data_to_array(result)
    if result.search('h2 a')
      title = result.search('h2 a').text.strip
      href = PRE_JOB_HREF + result.search('h2 a').attribute('href').text.strip

      @job_title_arr << title
      @job_href_arr << href
    else
      @job_title_arr << ''
      @job_href_arr << ''
    end
    
    if result.search('span span')[0]
      company_name = result.search('span span')[0].text.strip

      @company_name_arr << company_name
    else
      @company_name_arr << ''
    end
    
    if result.search('span span')[2]
      location = result.search('span span')[2].text.strip

      @location_arr << location
    else
      @location_arr << ''
    end

    if result.search('table span')
      job_description = result.search('table span').text.strip
      @job_description_arr << job_description
    else
      @job_description_arr << ''
    end
  end

  def insert_data_to_database
    joblink_arr = []

    @job_title_arr.each_with_index do |job_title, index|
      @joblink = Joblink.new
      @joblink.domain = @domain
      @joblink.keyword = @keyword
      @joblink.job_title = job_title
      @joblink.job_href = @job_href_arr[index]
      @joblink.company_name = @company_name_arr[index]
      @joblink.location = @location_arr[index]
      @joblink.job_description = @job_description_arr[index]
      joblink_arr.unshift(@joblink)
    end

    Joblink.import joblink_arr
  end

end
