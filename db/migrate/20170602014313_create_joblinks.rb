class CreateJoblinks < ActiveRecord::Migration[5.0]
  def change
    create_table :joblinks do |t|
      t.string :job_title
      t.string :company_name
      t.string :location
      t.string :job_description
      t.string :domain
      t.string :keyword
      t.string :job_href

      t.timestamps
    end
  end
end
