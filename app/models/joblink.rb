class Joblink < ApplicationRecord
  scope :order_joblink, -> { order('id DESC') }

  def self.to_csv
    attributes = %w{company_name job_title job_href domain keyword}

    CSV.generate(headers: true) do |csv|
      csv << %w{企業名 求人名 求人詳細url サイト名 検索キーワード}

      all.reverse.each do |joblink|
        csv << attributes.map { |attr| joblink.send(attr) }
      end
    end
  end
end
