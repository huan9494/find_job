class FindjobsController < ApplicationController

  include FindjobHelper

  def indeed
    if params[:query].present?
      initialize_job
      if @joblink
        update_job
      else
        create_job
      end
    end
  end

  def pagination
    session[:keyword] ||= ''
    @joblinks = Joblink.where(keyword: session[:keyword])
                       .order_joblink
                       .page(params[:page])
                       .per(10)
  end

  def download
    @joblink = Joblink.where(keyword: session[:keyword])
    respond_to do |format|
      format.csv { send_data @joblink.to_csv,
                   filename: "#{session[:keyword]}.csv" }
    end
  end

end