Rails.application.routes.draw do

  get 'findjobs/indeed'

  get 'findjobs/pagination'
  get 'findjobs/download'
  
  root 'findjobs#pagination'
end
