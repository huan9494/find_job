require 'test_helper'

class CsvControllerTest < ActionDispatch::IntegrationTest
  test "should get export" do
    get csv_export_url
    assert_response :success
  end

end
